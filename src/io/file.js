
const reader = require("xlsx");
const fs = require('fs');
const XlsxPopulate = require('xlsx-populate');


function writeFile(reportPath, read,sheetName = "Sheet1") {
 
    // Load an existing workbook
    XlsxPopulate.fromFileAsync(reportPath)
        .then(workbook => {
            // // Modify the workbook.
            // workbook.sheet(sheetName).cell("A1").value("This is neat!");
            // const cell = workbook.sheet(sheetName).cell("A1");
            // cell.style("fill", "b4c7dc");
            const cell = workbook.sheet(sheetName).row(1).cell(1);
            cell.style("fill", "b4c7dc");
            
            // Write to file.
            return workbook.toFileAsync(reportPath);

            // // Modify the workbook.
            // const value = workbook.sheet(sheetName).cell("A1").value();
            
            // // Log the value.
            // console.log(value);
        });
}


module.exports = {
    writeFile
};
  